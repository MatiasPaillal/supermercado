package com.example.demo.models;


public class Producto {
    private String nombre;
    private String seccion;
    private int precio;
    private int stock;
    private String ubicacion;

    public Producto(String nombre, String seccion, int precio, int stock, String ubicacion) {
        this.nombre = nombre;
        this.seccion = seccion;
        this.precio = precio;
        this.stock = stock;
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}

