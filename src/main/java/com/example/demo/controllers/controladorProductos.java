package com.example.demo.controllers;

import com.example.demo.models.Producto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class controladorProductos {

    private static List<Producto> listaProductos = new ArrayList<Producto>();

    @GetMapping("/Productos")
    public List<Producto> getProductos() {
        listaProductos.add(new Producto("Manzana", "frutas",100,300,"nose"));
        listaProductos.add(new Producto("peras", "frutas",100,300,"nose"));
        listaProductos.add(new Producto("tomate", "frutas",100,500,"nose"));
        listaProductos.add(new Producto("naranja", "frutas",100,400,"nose"));
        listaProductos.add(new Producto("cepillo", "Aseo",20,2000,"nose"));
        listaProductos.add(new Producto("iphone 13", "tecnologia",2,1000000,"nose"));
        listaProductos.add(new Producto("Samnsung Galaxy A30", "tecnologia",4,200000,"nose"));
        listaProductos.add(new Producto("Apio", "verdura",1,1000,"nose"));

        return listaProductos;
    }
}

