package com.example.demo.controllers;

import com.example.demo.models.Cajero;

import com.example.demo.models.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class controladorReponedor {

    private static List<Reponedor> listaProductos = new ArrayList<Reponedor>();

    @GetMapping("/Reponedor")
    public List<Reponedor> getProductos() {
        listaProductos.add(new Reponedor("Jose", "123.123.132-2","Jose P","112312300","Tecnologia"));
        listaProductos.add(new Reponedor("Pedro", "9.982.988-3","Pedro C","10adad0a","frutas"));
        listaProductos.add(new Reponedor("Matias", "14.111.323-5","Matias N","1231111aa00","frutas"));
        listaProductos.add(new Reponedor("Emanuel", "20.121.233-9","Emanuel ","llakksk","verduras"));
        listaProductos.add(new Reponedor("Andres", "7.122.763-2","Andres C","aaaaddasdasde","frutas"));

        return listaProductos;
    }
}

