package com.example.demo.controllers;

import com.example.demo.models.Cajero;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class controladorCajero {

    private static List<Cajero> listaProductos = new ArrayList<Cajero>();

    @GetMapping("/Cajero")
    public List<Cajero> getProductos() {
        listaProductos.add(new Cajero("Jose", "123.123.132-2","Jose P","112312300",300));
        listaProductos.add(new Cajero("Pedro", "9.982.988-3","Pedro C","10adad0a",300));
        listaProductos.add(new Cajero("Matias", "14.111.323-5","Matias N","1231111aa00",500));
        listaProductos.add(new Cajero("Emanuel", "20.121.233-9","Emanuel ","llakksk",400));
        listaProductos.add(new Cajero("Andres", "7.122.763-2","Andres C","aaaaddasdasde",2000));

        return listaProductos;
    }
}

