package com.example.demo.controllers;

import com.example.demo.models.Merma;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class controladorMerma {

    private static List<Merma> listaProductos = new ArrayList<Merma>();

    @GetMapping("/Merma")
    public List<Merma> getProductos() {
        listaProductos.add(new Merma("Manzana", "descomposicion",8));
        listaProductos.add(new Merma("peras", "golpes",10));
        listaProductos.add(new Merma("tomate", "descomposicion",4));


        return listaProductos;
    }
}

