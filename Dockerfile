FROM openjdk:17-alpine

VOLUME /tmp

EXPOSE 8080

COPY "./target/demo-0.0.1-SNAPSHOT.jar" "app.jar"

ENTRYPOINT ["java", "-jar", "app.jar"]

#sudo docker build -t demo .
#sudo docker run -p 8080:8080 -d demo